//
//  WeatherModel.swift
//  Weather-new
//
//  Created by Alexander Rozanov on 18.06.2020.
//  Copyright © 2020 Alexander Rozanov. All rights reserved.
//

import Foundation
import RealmSwift

class Weather: Object {
    @objc dynamic var cityName = ""
    override static func primaryKey() -> String? {
        return "cityName"
    }
}
