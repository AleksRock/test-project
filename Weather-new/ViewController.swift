//
//  ViewController.swift
//  Weather-new
//
//  Created by Alexander Rozanov on 02.06.2020.
//  Copyright © 2020 Alexander Rozanov. All rights reserved.
//
import Alamofire
import SwiftyJSON
import UIKit

class ViewController: UIViewController {

   
    
    
    
    
    
    
    
    @IBOutlet weak var tempHoursView: UIView!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    
    @IBOutlet weak var deskLabel: UILabel!
    
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var tempMaxLabel: UILabel!
    
    @IBOutlet weak var tempMinLabel: UILabel!
    
    @IBOutlet weak var weatherImage: UIImageView!
    
    
    @IBOutlet weak var currantTempLabel: UILabel!
    
    @IBOutlet weak var speedWind: UILabel!
    
    
    @IBOutlet weak var feelsIsTRemp: UILabel!
    
    @IBOutlet weak var sunRiseLabel: UILabel!
    
    
    @IBOutlet weak var sunSetLabel: UILabel!
    
    
    var cityName = ""
    var cityTemp = ""
    
    var latitude = 0.0
    var longitude = 0.0
    
    @IBOutlet weak var wetherImageOne: UIImageView!
    
    @IBOutlet weak var weatherImageTwo: UIImageView!
    
    @IBOutlet weak var weatherImageThree: UIImageView!
    
    @IBOutlet weak var weatherImageFour: UIImageView!
    
    
    @IBOutlet weak var nextTempLabelOne: UILabel!
    
    @IBOutlet weak var nextTempLabelTwo: UILabel!
    
    @IBOutlet weak var nextTempLabelThree: UILabel!
    
    @IBOutlet weak var nextTempLabelFour: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tempHoursView.layer.borderWidth = 1
        tempHoursView.layer.borderColor = UIColor.white.cgColor
        cityLabel.text = cityName
        
      //tempLabel.text = cityTemp
      //округление до целого на втором экране и добавление значка градуса С
        
        let cityTempDouble = Double(cityTemp)?.rounded()
        
        getWeather(city: cityName)
        detaiInfoWeather(city: cityName)
    }


    func getWeather(city: String) {
        
     let url = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
         AF.request(url, method: .get).validate().responseJSON { response in
                 switch response.result {
                 case .success(let value):
                     let json = JSON(value)
                     print("JSON: \(json)")
                     let tempDouble = json["main"]["temp"].doubleValue
                     let tempInt = lround(tempDouble)
                     self.tempLabel.text = "\(tempInt) C"
                     self.currantTempLabel.text = "\(tempInt)"
                     
                     self.deskLabel.text = json["weather"][0]["main"].stringValue
                    
                    // округление до целого градусов - вводится промежуточная константа temp1 которая принимает значенние Int а затем переводится в String
                     let tempMaxDouble = json["main"]["temp_max"].doubleValue
                        
                     let tempMinDouble = json["main"]["temp_min"].doubleValue
                     self.tempMaxLabel.text = "\(lround(tempMaxDouble))"
                     self.tempMinLabel.text = "\(lround(tempMinDouble))"
                    
                    let iconUrlString = "http://openweathermap.org/img/wn/\(json["weather"][0]["icon"].stringValue).png"
                    let iconUrl = URL(string: iconUrlString)
                    if let data = try? Data(contentsOf: iconUrl!) {
                        self.weatherImage.image = UIImage(data: data)
                    }
                    
//http://openweathermap.org/img/wn/\(json["weather"][0]["icon"].stringValue).png
                    
                     self.speedWind.text = json["wind"]["speed"].stringValue + "м/с"
                     let feelsTempDouble = json["main"]["feels_like"].doubleValue
                     self.feelsIsTRemp.text = "Ощущается температура на \(lround(feelsTempDouble)) C"
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = .none
                    dateFormatter.timeStyle = .medium
                    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
                    dateFormatter.locale = .current
                     
                    let sunriseInt = json["sys"]["sunrise"].intValue
                    let sunsetInt = json["sys"]["sunset"].intValue
                    let sunriseDate = Date(timeIntervalSince1970: TimeInterval(sunriseInt))
                    let sunsetDate = Date(timeIntervalSince1970: TimeInterval(sunsetInt))
                    
            self.sunRiseLabel.text = dateFormatter.string(from: sunriseDate)
            self.sunSetLabel.text = dateFormatter.string(from: sunsetDate)
                    
                     self.latitude = json["coord"]["lat"].doubleValue
                     self.longitude = json["coord"]["lon"].doubleValue
                    
                    
                 case .failure(let error):
                     print(error)
       // вывод на  экран через Alert сообщения об ошибке ввода
                  
                 }
             }
         }
    func detaiInfoWeather(city: String) {
        let url = "https://api.openweathermap.org/data/2.5/forecast?q=\(city)&units=metric&appid=6da8e8bdb22e1d408ffb437eab399b45"
        AF.request(url, method: .get).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                let json = JSON(value)
                
                
                
                let weatherImageArray = [self.wetherImageOne,self.weatherImageTwo,self.weatherImageThree,self.weatherImageFour]
                let nextTempArray = [self.nextTempLabelOne,self.nextTempLabelTwo,self.nextTempLabelThree,self.nextTempLabelFour]
                
                
                
                for number in 0...3 {
                    let iconUrlString = "http://openweathermap.org/img/wn/\(json["list"][number]["weather"][0]["icon"].stringValue).png"
                    
                let iconUrl = URL(string: iconUrlString)
                if let data = try? Data(contentsOf: iconUrl!) {
                    weatherImageArray[number]?.image = UIImage(data: data)
                  
                }
                    let nextTempDouble = json["list"][number]["main"]["temp"].doubleValue
                    nextTempArray[number]?.text = "\(lround(nextTempDouble))"
                    
                    print(weatherImageArray)            //self.wetherImageOne.image =
                    
                    
                    
                    
                    
                    }
                
                
                case .failure(let error):
                print(error)


//}
    
   
                
                
    
 
                
        
        
        
        

    //override func prepare(for segue: UIStoryboardSegue,  sender: Any?) {
   //    if segue.identifier == "onMap" {
   //     var detailViewController = segue.destination as? MapViewController {
                
   //             detailViewController?.latitude = self.latitude
   //             detailViewController?.longitude = self.longitude
    
    }
        
}
}
    
    override func prepare(for segue: UIStoryboardSegue,  sender: Any?) {
    if segue.identifier == "onMap" {
        var mapViewController = segue.destination as! MapViewController
       mapViewController.latitude = self.latitude
           mapViewController.longitude = self.longitude
        }
    
}
}
