//
//  TableViewController.swift
//  Weather-new
//
//  Created by Alexander Rozanov on 02.06.2020.
//  Copyright © 2020 Alexander Rozanov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class TableViewController: UITableViewController {

    struct City {
    
    var name = ""
    var temp = ""
    var time = ""
        
    }
    var citiesArray = [City]()
    var cityName = ""
    var cityTemp = ""
    let realm = try! Realm()
    var results: Results<Weather>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        updateAll()
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return citiesArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCity", for: indexPath) as! TableViewCell
        
        cell.cityLabel.text = citiesArray[indexPath.row].name
        cell.timeLabel.text = citiesArray[indexPath.row].time
        cell.tempLabel.text = citiesArray[indexPath.row].temp
        // Configure the cell...точно существует и теперь мы можем использовать 3 ячейки
        cell.layer.borderWidth=1
        cell.layer.borderColor=UIColor.black.cgColor
        

        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0 }
        
    
      override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityName = citiesArray[indexPath.row].name
        performSegue(withIdentifier: "toDetail", sender: nil)
        
    }
        override func prepare(for segue: UIStoryboardSegue,  sender: Any?) {
        if segue.identifier == "toDetail" {
            let detailViewController = segue.destination as! ViewController
            detailViewController.cityName = self.cityName
            detailViewController.cityTemp = self.cityTemp
    }
    }
    
    //override func prepare(for : UIStoryboardSeque, sender: Any?) {
    //if seque.identifier == "to Detail" {
    //if let detailViewController = seque.desination as ViewController {
  //  detailViewController = self.cityName

        
        /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    
    */

   
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            results = realm.objects(Weather.self)
            
                       try! self.realm.write {
                        self.realm.delete((results?[indexPath.row])!)
            }
            
            self.citiesArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
  

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    */
  // вводится дополнительная функция
    func getCurrentWeather(city: String) {
    let url = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
    AF.request(url, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                
                let weather = Weather()
                weather.cityName = city
                try! self.realm.write {
                    self.realm.add(weather, update: .modified)
                }
                print(self.realm.objects(Weather.self).count)
// округление до целого градусов - вводится промежуточная константа temp1 которая принимает значенние Int а затем переводится в String
                
                let temp = json["main"]["temp"].doubleValue
               
                
                let cityTime = json["dt"].intValue
                let date = Date(timeIntervalSince1970: TimeInterval(cityTime))
                
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .none
                dateFormatter.timeStyle = .medium
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
                dateFormatter.locale = .current
                let currentTime = dateFormatter.string(from: date)
                self.citiesArray.append(City(name: city,temp: "\(lround(temp)) °C" , time: "\(currentTime)"))
                
                self.tableView.reloadData()
                
                
                case .failure(let error):
                print(error)
  // вывод на  экран через Alert сообщения об ошибке ввода
                let errorAlert = UIAlertController(title: "Ошибка", message: "Вы ввели неправильно город", preferredStyle: .alert)
                let actionClose = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
                errorAlert.addAction(actionClose)
                self.present(errorAlert,animated: true, completion: nil)
            }
        }
        
    }
    // // ввод через Alert
    
    @IBAction func addCity(_ sender: Any) {
 
            let alert = UIAlertController(title: "Add city", message: "", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Example: Москва"
            }
            let actionOk = UIAlertAction(title: "OK", style: .default) { (okAction) in
                let cityNameFromFextFild = alert.textFields![0].text!
                self.getCurrentWeather(city: cityNameFromFextFild)
}
// ввод кнопки cancel
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            
            self.present(alert, animated: true, completion: nil)
            
                 }
    
    func updateAll() {
        let weather = realm.objects(Weather.self)
        for number in 0..<weather.count {
            getCurrentWeather(city: weather[number].cityName)
        }
    }
    
}
    //    let alertController = UIAlertController(title: "Add City", message: "", preferredStyle: .alert)
  //           alertController.addTextField {(textField) in textField.placeholder = "Examle Moscow"
  //                       }
//        let actionOk = UIAlertAction(title: "Ok", style: .default) {(OkAction) in
 //           let cityNameFromTextField = alertController.textFields![0].text
 //           self.citiesArray.append(City(name: cityNameFromTextField!, temp: "", time: ""))
         
             
 //            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
  //           alertController.addAction(actionOk)
  //           alertController.addAction(actionCancel)
  //           self.present(alertController, animated: true, completion: nil)
 //            self.tableView.reloadData()
        
 //   }
        
    
    

